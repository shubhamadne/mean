// require the express package
const express = require('express')

// create a server instance
const app = express()

// middleware
function log3(request, response, next) {
  console.log('inside function log 3')

  console.log(`method: ${request.method}`)
  console.log(`url: ${request.url}`)

  next()
}

// middleware
function log1(request, response, next) {
  console.log('inside function log 1')

  console.log(`method: ${request.method}`)
  console.log(`url: ${request.url}`)

  next()
}

// middleware
function log2(request, response, next) {
  console.log('inside function log 2')

  console.log(`method: ${request.method}`)
  console.log(`url: ${request.url}`)

  next()
}

app.use(log3)
app.use(log2)
app.use(log1)

app.get('/', (request, response) => {
  console.log('inside GET /')
  response.end('this is GET /')
})

// listen on port 3000 (start the server)
app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})