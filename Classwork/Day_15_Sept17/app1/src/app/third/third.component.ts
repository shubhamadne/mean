import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css']
})
export class ThirdComponent implements OnInit {

  // 1 : red, 2 : green, 3 : blue

  boxType = 3;

  category = 4;

  type = 5;

  constructor() { }

  ngOnInit(): void {
  }

}
