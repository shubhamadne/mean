import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fourth',
  templateUrl: './fourth.component.html',
  styleUrls: ['./fourth.component.css']
})
export class FourthComponent implements OnInit {

  myClick()
  {
    alert("You clicked button");
  }

  // 1 : red, 2 : green, 3 : blue
  boxType = 1;

  changeColorRed(){ this.boxType = 1 }
  changeColorGreen(){ this.boxType = 2 }
  changeColorBlue(){ this.boxType = 3 }

  constructor() { }

  ngOnInit(): void {
  }

}
