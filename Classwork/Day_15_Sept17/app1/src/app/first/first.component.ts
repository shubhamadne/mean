import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  firstName = "Shubham"
  lastName = "adne" 

  person = {
    name : "shubham",
    age : 27,
    phone : 1234567890
  }
  constructor() { }

  ngOnInit(): void {
  }

}
