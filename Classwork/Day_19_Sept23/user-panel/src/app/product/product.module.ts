import { ProductService } from './product.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { CartComponent } from './cart/cart.component';
import { GallaryComponent } from './gallary/gallary.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [CartComponent, GallaryComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    HttpClientModule
  ],
  providers : [ProductService]
})
export class ProductModule { }
