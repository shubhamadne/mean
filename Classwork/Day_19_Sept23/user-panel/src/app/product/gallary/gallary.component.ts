import { ToastrService } from 'ngx-toastr';
import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-gallary',
  templateUrl: './gallary.component.html',
  styleUrls: ['./gallary.component.css']
})
export class GallaryComponent implements OnInit {

  products = []
  allProducts = []

  constructor(
    private toastr : ToastrService,
    private cartService : CartService,
    private productService : ProductService
  ) { }

  ngOnInit(): void {
    this.loadProducts()
  }

  loadProducts() {
    this.productService
      .getProducts()
      .subscribe(response => {
        console.log(response)
        if (response['status'] == 'success') {
          this.allProducts = response['data']
          this.products = this.allProducts
        }
      })
  }

  addToCart(product) {
    this.cartService
      .addCartItem(product['id'], product['price'], 1)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('added your product to cart')
        }
      })
  }
}
