//signup.component.js

firstName = ''
  lastName = ''
  email = ''
  password = ''

  constructor( private userService : UserService) { }

  ngOnInit(): void {
  }

  onSignup()
  {
    this.userService.userSignup(this.firstName, this.lastName, this.email, this.password)
    .subscribe(response => {
      if(response['status'] == 'success')
      {
        alert('You have Registered Successfully')
      }
      else
      {
        console.log(response['error'])
      }
    })
  }

  //login component.js
  email = ''
  password = ''

  constructor( private userService : UserService) { }

  ngOnInit(): void {
  }

  onLogin()
  {
    this.userService.userLogin(this.email, this.password)
    .subscribe(response => {
      if(response['status'] == 'success')
      {
        alert('Welcome')
      }
      else
      {
        console.log(response['error'])
      }
    })
  }

  //userService.js

  url = 'http://localhost:4000/user'
  
  constructor( private httpClient : HttpClient) { }

  userSignup(firstName : string, lastName : string, email : string, password : string)
  {
    const body = {
      firstName : firstName,
      lastName : lastName,
      email : email,
      password : password
    }
    return this.httpClient.post(this.url + '/signup', body)
  }

  userLogin(email : string, password : string)
  {
    const body = {
      email : email,
      password : password
    }
    return this.httpClient.post(this.url + '/signin', body)
  }

  //user routing module.js

  { path : 'login', component : LoginComponent},
  { path : 'signup', component : SignupComponent},
  { path : 'profile', component : ProfileComponent},
  { path : 'address-add', component : AddressAddComponent},
  { path : 'address-list', component : AddressListComponent}

  //app routing module .ts

  {
    path : 'user',
    component : UserModule,
    children : [
      { path : 'signup', component : SignupComponent},
      { path : 'login', component : LoginComponent},
      { path : 'profile', component : ProfileComponent},
      { path : 'address-list', component : AddressAddComponent},
      { path : 'address-add', component : AddressAddComponent}
    ]
  },
  {
    path : 'product',
    component : ProductModule,
    children : [
      { path : 'cart', component : CartComponent},
      { path : 'gallary', component : GallaryComponent}
    ]
  },
  {
    path : 'order',
    component : OrderModule,
    children : [
      { path : 'order-history', component : OrderHistoryComponent},
      { path : 'preview', component : PreviewComponent}
    ]
  }