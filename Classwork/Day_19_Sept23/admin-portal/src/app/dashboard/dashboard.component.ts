import { logging } from 'protractor';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( 
    private productService : ProductService,
    private orderService : OrderService,
    private userService : UserService) { }

  users = []
  products = []
  orders = []

  ngOnInit(): void {
    this.getTotalUsers()
    this.getTotalProducts()
    this.getTotalOrders()
  }

  getTotalUsers()
  {
    this.userService.getUsers().subscribe(response => {
      if(response['status'] == 'success')
      {
        this.users = response['data']
        console.log(this.users.length)
      }
      else
      {
        console.log(response['error'])
      }
    })
  }

  getTotalProducts()
  {
    this.productService.getProducts().subscribe(response => {
      if(response['status'] == 'success')
      {
        this.products = response['data']
      }
      else
      {
        console.log(response['error'])
      }
    })
  }

  getTotalOrders()
  {
    this.orderService.getOrders().subscribe(response => {
      if(response['status'] == 'success')
      {
        this.orders = response['data']
      }
      else
      {
        console.log(response['error'])
      }
    })
  }
}
