import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  url = 'http://localhost:3000/order'
  constructor(
    private httpClient : HttpClient
  ) { }

  getOrders()
  {
    const httpOptions = {
      headers : new HttpHeaders({
        token : sessionStorage['token']
      })
    }

    return this.httpClient.get(this.url, httpOptions)
  }
}
