import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminService implements CanActivate {

  url = 'http://localhost:3000/admin'

  constructor( 
    private router : Router,
    private httpCliect : HttpClient) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    if(sessionStorage['token'])
    {
    //user is already logged in
    //launch the component
    return true
    }else{
      this.router.navigate['/login']
    //user is not logged in
    // dont launch the component
    return false
    }
  }

  adminLogin(email : string, password : string)
  {
    const body = {
      email : email,
      password : password
    }

    return this.httpCliect.post(this.url + '/signin', body)
  }
}
