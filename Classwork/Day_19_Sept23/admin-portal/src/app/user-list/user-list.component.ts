import { UserService } from '../user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users = []

  constructor(private userService : UserService) { }

  ngOnInit(): void {
    this.loadUsers()
  }

  loadUsers()
  {
    this.userService.getUsers().subscribe(response => {
      if(response['status'] == 'success')
      {
        this.users = response['data']
        this.loadUsers()
      }
      else
      {
        console.log(response['error'])
      }
    })
  }

  activateUser(user)
  {
    this.userService.changeActivateStatus(user).subscribe(response => {
      if(response['status'] == 'success')
      {
        this.loadUsers()
      }
      else
      {
        console.log(response['error'])
      }
    })
  }
}
