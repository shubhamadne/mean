import { OrderService } from './order.service';
import { BrandService } from './brand.service';
import { CategoryService } from './category.service';
import { ProductService } from './product.service';
import { AdminService } from './admin.service';
import { UserService } from './user.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { OrderListComponent } from './order-list/order-list.component'
@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    LoginComponent,
    ProductListComponent,
    ProductAddComponent,
    DashboardComponent,
    UploadImageComponent,
    OrderListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    UserService,
    AdminService,
    ProductService,
    CategoryService,
    BrandService,
    OrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
