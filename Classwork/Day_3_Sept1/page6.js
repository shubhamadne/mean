const fs = require('fs');

function synchronusReadFile() 
{
    console.log('File Reading Started....');
    try {
        // blocking call/API
        const data = fs.readFileSync('./file2.txt')
        console.log('file reading finished')
        console.log(`data = ${data}`)
        console.log('bye bye')
      } catch(ex) {
        console.log(`exception: ${ex}`)
      }

    // Maths Calculation
    console.log('peforming multiplication')
    const result = 24524 * 34532543
    console.log(`result = ${result}`)

    // mathematical calculation
    console.log('peforming division')
    const result2 = 23412424524524 / 3453245245232543
    console.log(`result2 = ${result2}`)
    
}

//synchronusReadFile();

function asynchronusReadFile() 
{
    console.log('File Reading Started....');
    // starts a thread to perform the read operation
    fs.readFile('./file1.txt' , (error, data) => {
        console.log('File Reading Finished....');
        if (error) 
        {
            console.log(`error: ${error}`)
        } 
        else 
        {
            console.log(`data = ${data}`)
        }
        console.log('tata.. bye.. bye..');
    });


    // Maths Calculation
    console.log('peforming multiplication')
    const result = 24524 * 34532543
    console.log(`result = ${result}`)
}

//asynchronusReadFile();

function funtion1() {
    console.log('download started')
    setTimeout(() => {
      console.log('download finished')
    }, 5000)
  
    // perform mathematical operation
    console.log('performing mulitplication')
    const result =  244534433534 * 23424243242243
    console.log(`answer = ${result}`)  
  }
  
  //funtion1()

  function myReadFile(path, func) {
    // func =  (error, data) => { .. }
    const data = fs.readFileSync(path)
    setTimeout(() => {
      func(null, data)
    }, 10000)
  }
  
  function function2() {
    console.log('reading started')
    myReadFile('./file1.txt', (error, data) => {
      console.log(`data = ${data}`)
      console.log('reading finished')
    })
  
    // perform mathematical operation
    console.log('performing mulitplication')
    const result =  244534433534 * 23424243242243
    console.log(`answer = ${result}`)
  }
  
  // function2()