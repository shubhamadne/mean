// importing a system (node) module
const os = require('os');

//console.log(os);

// CPU Architecture
console.log(`architecture: ${os.arch()}`)
// OS
console.log(`os: ${os.platform()}`)
// Host Name
console.log(`Host Name: ${os.hostname()}`)
// Home Directory
console.log(`Home Directory: ${os.homedir()}`)
// Total Memory
console.log(`Total Memory: ${os.totalmem()}`)
// Free Memory
console.log(`Free Memory: ${os.freemem()}`)
//CPUS
console.log(` CPUS `);
console.log(os.cpus());
//Network Interface
console.log(` Network Interface `);
console.log(os.networkInterfaces());
