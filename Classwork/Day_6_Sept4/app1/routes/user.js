
const express = require('express');

// router is used to handle the route
const router = express.Router();

router.get('/', (request, response) => {
    console.log('inside GET /user')
    response.send('GET /user')
})

router.post('/', (request, response) => {
console.log('inside POST /user')
response.send('post /user')
})

// the exported router can be imported and added into the server
module.exports = router;