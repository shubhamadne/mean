
const express = require('express');

// router is used to handle the route
const router = express.Router();

router.get('/', (request, response) => {
    console.log('inside GET /note')
    response.send('GET /note')
})

router.post('/', (request, response) => {
console.log('inside POST /note')
response.send('POST /note')
})


// the exported router can be imported and added into the server
module.exports = router;