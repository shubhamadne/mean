function function1() {
    const employees = [
      {
        id: 1,
        name: 'employee 1',
        department: 'computer',
        salary: 15.50,
        role: 'developer'
      },
      {
        id: 2,
        name: 'employee 2',
        department: 'computer',
        salary: 10.50,
        role: 'tester'
      },
      {
        id: 3,
        name: 'employee 3',
        department: 'computer',
        salary: 25.50,
        role: 'architect'
      }
    ]
  
    // for loop 1
    // for (let index = 0; index < employees.length; index++) {
    //   const employee = employees[index];
    //   console.log(employee)
    // }
  
    // for loop 2
    // employees.forEach(employee => {
    //   console.log(employee)
    // })
  
    // for loop 3
    for (let employee of employees) {
      console.log(employee)
    }
  
    // for loop 4
    // for (let employee in employees) {
    //   console.log(employee)
    // }
  
  }
  
  function1()