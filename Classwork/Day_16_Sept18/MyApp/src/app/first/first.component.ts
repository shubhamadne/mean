import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  //1 - red
  //2 - green
  //3 - blue
  boxType = 1;

  redColor()
  {
    //alert("Red button clicked");
    this.boxType = 1;
  }
  greenColor()
  {
    this.boxType = 2;
  }
  blueColor()
  {
    this.boxType = 3;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
