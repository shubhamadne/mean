import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CarListComponent } from './car-list/car-list.component';
import { MobileListComponent } from './mobile-list/mobile-list.component';
import { AccordionComponent } from './accordion/accordion.component';
import { RatingComponent } from './rating/rating.component';
import { SmileyComponent } from './smiley/smiley.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    EmployeeListComponent,
    CarListComponent,
    MobileListComponent,
    AccordionComponent,
    RatingComponent,
    SmileyComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
