import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employee = {
    id : 1,
    name : "Employee 1",
    department : "Design",
    role : "Developed",
    salary : 35000
  }

  employees = [
    {
      id : 1,
      name : "Employee 1",
      department : "Design",
      role : "Developer",
      salary : 35000
    },
    {
      id : 2,
      name : "Employee 2",
      department : "Design",
      role : "tester",
      salary : 40000
    },
    {
      id : 3,
      name : "Employee 3",
      department : "Design",
      role : "Developed",
      salary : 50000
    }
]
  constructor() { }

  ngOnInit(): void {
  }

}
