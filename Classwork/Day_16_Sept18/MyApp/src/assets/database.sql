
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS collegeRegistration;
DROP TABLE IF EXISTS studentRegistraton;
DROP TABLE IF EXISTS studentEducationalDetails;
DROP TABLE IF EXISTS workExperience;
DROP TABLE IF EXISTS companyRegistration;
DROP TABLE IF EXISTS jobVacancies;

create table user
(
userId int primary key auto_increment,
role varchar(50),
name varchar(100),
contact varchar(20),
email varchar(50),
password varchar(50),
city varchar(100),
streetName varchar(100)
);

create table collegeRegistration
(
userId int ,
collegeId varchar(50) primary key,
university varchar(100),
tpoName varchar(100),
foreign key(userId) references user(userId)
);


create table studentRegistraton
(
userId int ,
collegeId varchar(50),
firstName varchar(100),
lastName varchar(100),
rollNo varchar(10) not null,
birthDate date,
age int,
gender varchar(10),
placed varchar(100) default false,
foreign key(userId) references user(userId),
foreign key(collegeId) references collegeRegistration(collegeId)
);



create table studentEducationalDetails
(
userId int ,
education varchar(100) not null,
passingYear year(4),
schoolCollegeName varchar(100),
university varchar(200),
aggregateMarks int not null,
projectName varchar(200),
projectDescription varchar(200),
achievements varchar(500),
extraCurricural varchar(500),
foreign key(userId) references user(userId)
);

create table workExperience
(
userId int,
companyName varchar(100) not null,
experience varchar(10),
startDate date,
endDate date,
foreign key(userId) references user(userId)
);

create table companyRegistration
(
userId int,
companyId varchar(50) primary key,
establishedYear year(4),
hrName varchar(100),
foreign key(userId) references user(userId)
);

create table jobVacancies
(
companyId varchar(50),
jobId varchar(50) primary key,
jobTitle varchar(100),
jobDescription varchar(200),
jobDesignation varchar(200),
qualification varchar(100),
experience varchar(20),
location varchar(20),
foreign key(companyId) references companyRegistration(companyId)
);


COMMIT;
