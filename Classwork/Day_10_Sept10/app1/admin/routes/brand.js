const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------
/**
 * @swagger
 *
 * /brand:
 *   get:
 *     description: For getting list of brands
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */
router.get('/', (request, response) => {
  const statement = `select id, title, description from brand`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------

/**
 * @swagger
 *
 * /brand:
 *   post:
 *     description: For adding New Brand
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of Brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Information about Brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.post('/', (request, response) => {
  const {title, description} = request.body
  const statement = `insert into brand (title, description) values ('${title}', '${description}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
/**
 * @swagger
 *
 * /brand:
 *   put:
 *     description: For editing existing Brand
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of Brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Information about Brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.put('/:brandId', (request, response) => {
  const { brandId } = request.params;
  const { title, description } = request.body;

  const statement = `update brand set title = '${title}', description = '${description}' WHERE id = '${brandId}'`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------
/**
 * @swagger
 *
 * /brand:
 *   delete:
 *     description: For deleting existing Brand
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.delete('/:brandId', (request, response) => {
  const { brandId } = request.params;

  const statement = `delete from brand where id = ${brandId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router