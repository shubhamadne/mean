const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------
/**
 * @swagger
 *
 * /review:
 *   get:
 *     description: For getting all Reviews
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.get('/:productId', (request, response) => {
  const {productId} = request.params
  const statement = `select id, userId, review, rating from productReviews where productId = ${productId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------
// DELETE
// -------------------------------------------------
/**
 * @swagger
 *
 * /review:
 *   delete:
 *     description: For deleteing unwanted Reviews
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from productReviews where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router