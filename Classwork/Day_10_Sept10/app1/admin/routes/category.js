const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category:
 *   get:
 *     description: For Getting all categories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.get('/', (request, response) => {
  const statement = `select id, title, description from category`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category:
 *   post:
 *     description: For adding new Category
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Information about category
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.post('/', (request, response) => {
  const {title, description} = request.body
  const statement = `insert into category (title, description) values ('${title}', '${description}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category:
 *   put:
 *     description: For editing existing Brand
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of Brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Information about Brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.put('/:Id', (request, response) => {
  const { Id } = request.params;
  const { title, description } = request.body;

  const statement = `update category set title = '${title}', description = '${description}' WHERE id = '${Id}'`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------
/**
 * @swagger
 *
 * /category:
 *   delete:
 *     description: For deleting existing Brand
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.delete('/:Id', (request, response) => {
  const { Id } = request.params;

  const statement = `delete from category where id = ${Id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router