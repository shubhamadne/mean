const express = require('express')
const utils = require('../../utils')
const db = require('../../db')

// multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: 'images/' })

const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------
/**
 * @swagger
 *
 * /product:
 *   get:
 *     description: For getting all Products
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.get('/', (request, response) => {
  const statement = `
      select p.id, p.title, p.description, 
        c.id as categoryId, c.title as categoryTitle,
        b.id as brandId, b.title as brandTitle,
        p.price, p.image, p.isActive from product p
      inner join category c on c.id = p.category
      inner join brand b on b.id = p.brand
  `
  db.query(statement, (error, data) => {
    if (error) {
      response.send(utils.createError(error))
    } else {
      // empty products collection
      const products = []

      // iterate over the collection and modify the structure
      for (let index = 0; index < data.length; index++) {
        const tmpProduct = data[index];
        const product = {
          id: tmpProduct['id'],
          title: tmpProduct['title'],
          description: tmpProduct['description'],
          price: tmpProduct['price'],
          isActive: tmpProduct['isActive'],
          brand: {
            id: tmpProduct['brandId'],
            title: tmpProduct['brandTitle']
          },
          category: {
            id: tmpProduct['categoryId'],
            title: tmpProduct['categoryTitle']
          }
        }
        products.push(product)
      }

      response.send(utils.createSuccess(products))
    }

  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
/**
 * @swagger
 *
 * /product/upload-image:
 *   post:
 *     description: For adding new image of Product
 *     produces:
 *       - application/json
 *     parameters:
 *     responses:
 *       200:
 *         description: login
 */
router.post('/upload-image/:productId', upload.single('image'), (request, response) => {
  const {productId} = request.params
  const fileName = request.file.filename

  const statement = `update product set image = '${fileName}' where id = ${productId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})
/**
 * @swagger
 *
 * /product/create:
 *   post:
 *     description: For Adding new Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Information about Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: category
 *         description: Category of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: price
 *         description: Price of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: brand
 *         description: Brand of Product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.post('/create', (request, response) => {
  const {title, description, category, price, brand} = request.body
  const statement = `insert into product (title, description, category, price, brand) values (
    '${title}', '${description}', '${category}', '${price}', '${brand}'
  )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
/**
 * @swagger
 *
 * /product:
 *   put:
 *     description: For editing existing Product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Information about Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: category
 *         description: Category of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: price
 *         description: Price of Product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: brand
 *         description: Brand of Product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.put('/:id', (request, response) => {
  const {id} = request.params
  const {title, description, category, price, brand} = request.body
  const statement = `update product set 
      title = '${title}',
      description = '${description}',
      category = '${category}',
      price = '${price}',
      brand = '${brand}'
  where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

/**
 * @swagger
 *
 * /product/update-state:
 *   put:
 *     description: For updating state of Product (0 or 1)
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: isActive
 *         description: 1 - Active and 0 - Not Active
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.put('/update-state/:id/:isActive', (request, response) => {
  const {id, isActive} = request.params
  const statement = `update product set 
      isActive = ${isActive}
    where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------
/**
 * @swagger
 *
 * /product:
 *   delete:
 *     description: For deleting existing Product
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.delete('/:id', (request, response) => {
  const {id} = request.params
  const statement = `delete from product where id = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router