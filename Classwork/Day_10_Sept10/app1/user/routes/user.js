const express = require('express')
const db = require('../../db')
const config = require('../../config')
const utils = require('../../utils')
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken');
const mailer = require('../../mailer')

const router = express.Router()

// ---------------------------------------
//                  GET
// ---------------------------------------



// ---------------------------------------
//                  POST
// ---------------------------------------
/**
 * @swagger
 *
 * /user/signup:
 *   post:
 *     description: For signing up an user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: first name of user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: last name of user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: email of user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: user's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.post('/signup', (request, response) => {
  const {firstName, lastName, email, password} = request.body
  
  const statement = `insert into user (firstName, lastName, email, password) values (
    '${firstName}', '${lastName}', '${email}', '${crypto.SHA256(password)}'
  )`
  db.query(statement, (error, data) => {

    mailer.sendEmail(email, 'Welcome to mystore', '<h1>welcome</h1>',  (error, info) => {
      console.log(error)
      console.log(info)
      response.send(utils.createResult(error, data))
    })

  })
})
/**
 * @swagger
 *
 * /user/signin:
 *   post:
 *     description: For signing in of user
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email of user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: user's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.post('/signin', (request, response) => {
  const {email, password} = request.body
  const statement = `select id, firstName, lastName from user where email = '${email}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({status: 'error', error: error})
    } else {
      if (users.length == 0) {
        response.send({status: 'error', error: 'user does not exist'})
      } else {
        const user = users[0]
        const token = jwt.sign({id: user['id']}, config.secret)
        response.send(utils.createResult(error, {
          firstName: user['firstName'],
          lastName: user['lastName'],
          token: token
        }))
      }
    }
  })
})


// ---------------------------------------
//                  PUT
// ---------------------------------------




// ---------------------------------------
//                  DELETE
// ---------------------------------------



module.exports = router