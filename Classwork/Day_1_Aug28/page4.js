// find the even numbers from the collection

function function1() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    for (let index = 0; index < numbers.length; index++) {
      const number = numbers[index];
      if (number % 2 == 0) {
        console.log('even number')
      } else {
        console.log('odd number')
      }
    }
  }
  
  // function1()
  
  function function2() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  
    for (let index = 0; index < numbers.length; index++) {
      const number = numbers[index];
      if (number % 2 == 0) {
        console.log(number)
      }
    }
  }
  
  // function2()
  
  function function3() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const evens = []
    for (let index = 0; index < numbers.length; index++) {
      const number = numbers[index];
      if (number % 2 == 0) {
        evens.push(number)
      }
    }
  
    console.log(numbers)
    console.log(evens)
  }
  
  // function3()
  
  
  function isEven(number) 
  {
    // return number % 2 == 0;
    if(number % 2 == 0)
        return number;
  }
  function isOdd(number) 
  {
    // return number % 2 == 0;
    if(number % 2 != 0)
        return number;
  }
  
  function function4() {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const evens = []
    for (let index = 0; index < numbers.length; index++) {
      const number = numbers[index];
      if (isEven(number)) {
        evens.push(number)
      }
    }
  
    console.log(numbers)
    console.log(evens)
  }
  
  //function4()

  function function5() 
  {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    // const evens = numbers.map(isEven)
    const evens = numbers.filter(isEven)
    const odds = numbers.filter(isOdd)
    console.log(numbers)
    console.log(evens)
    console.log(odds)
  }
  
  //function5()

  function offord(car)
  {
      //return car['price'] < 10
      if( car["price"] < 10)
      return {model : car["model"], price : car["price"], company : car["company"], color : car["color"]};
  }
  function function6() 
  {
    const cars = [
      { model: "i20", price: 7.5, company: "hyundai", color: "space gray"},
      { model: "nano", price: 2.5, company: "tata", color: "yellow"},
      { model: "x5", price: 35.5, company: "BMW", color: "dark blue"}
    ]
  
    //const car = cars.map(offord);
    const car = cars.filter(offord);

    console.log(cars);
    console.log(car);
  }
  //function6();

  function canVote(person)
  {
      return person['age'] >= 18
      /*if(person["age"] >= 18)
        return {name : person["name"], email : person["email"], age : person["age"]}; */
  }
  function function7() {
    const persons = [
      { name: "person1", email: "person1@test.com", age: 14 },
      { name: "person2", email: "person2@test.com", age: 45 },
      { name: "person3", email: "person3@test.com", age: 50 },
      { name: "person4", email: "person4@test.com", age: 16 }
    ]
  
    const newPersons = persons.filter(canVote);
    console.log(persons)
    console.log(newPersons)
  }
  //function7();

  
