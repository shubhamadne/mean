function function1() {

    //Array of Numbers
    const numbers = [10, 20, 30, 40, 50];
    console.log(numbers);
    console.log(`type of numbers = ${typeof(numbers)}`);

    //Array of string
    const countries = ["india", "usa", "uk"];
    console.log(countries);
    console.log(`type of countries = ${typeof(countries)}`);

    //Array of Objects
    const person = [
        { "name" : "person1", age : 20 },
        { "name" : "person2", age : 24 },
    ];
    console.log(person);
    console.log(`type of person = ${typeof(person)}`);
  }
  
  //function1();

  function add(p1, p2) 
  {
    console.log(`addition = ${p1 + p2}`);
  }
  
  
  function execute(func) 
  {
    // func = add
    console.log('inside execute')
    func(10, 20)
  }
  
  execute(add)

