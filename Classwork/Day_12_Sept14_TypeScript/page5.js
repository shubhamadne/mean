// implicit
function add(p1, p2) {
    console.log("addition = " + (p1 + p2));
}
// add(10, '20')
// explicit
function subtract(p1, p2) {
    console.log("subtraction = " + (p1 - p2));
}
// subtract(20, 30)
// subtract("tes5", "test2")
// int square(int num) { return num * num; }
function square(num) {
    return num * num;
}
console.log(square(5));
