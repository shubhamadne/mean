import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = 'http://localhost:3000/product'

  httpClient : HttpClient

  constructor(httpClient : HttpClient) 
  { 
    this.httpClient = httpClient
  }

  getProduct()
  {
    return this.httpClient.get(this.url)
  }

  createProduct(title : string, description : string, price : number, category : number, brand : number)
  {
    const body = {
      title : title,
      description : description,
      price : price,
      category : category,
      brand : brand
    }

    return this.httpClient.post(this.url, body)
  }

  deleteProduct(id : number)
  {
    return this.httpClient.delete(this.url + "/" + id)
  }
}
