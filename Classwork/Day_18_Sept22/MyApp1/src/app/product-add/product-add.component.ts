import { CategoryService } from '../category.service';
import { BrandService } from '../brand.service';
import { ProductService } from '../product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {

  title = ''
  description = ''
  price = 0
  category = 1
  brand = 1

  categories = []
  brands = []

  productService : ProductService
  brandService : BrandService
  categoriService : CategoryService

  constructor(
    productService : ProductService,
    brandService : BrandService,
    categoriService : CategoryService
    ) 
  { 
    this.productService = productService
    this.categoriService = categoriService
    this.brandService = brandService
  }

  ngOnInit(): void {
    this.loadData()
  }

  loadData()
  {
    this.categoriService.getCategory().subscribe(response => {
      if(response['status'] == 'success')
      {
        this.categories = response['data']
      }
      else
      {
        console.log(response['error'])
      }
    })

    this.brandService.getBrands().subscribe(response => {
      if(response['status'] == 'success')
      {
        this.brands = response['data']
      }
      else
      {
        console.log(response['error'])
      }
    })
  }
  onSave()
  {
    const request = this.productService.createProduct(this.title, this.description, this.price, this.category, this.brand)
    request.subscribe(response => {
      if(response['status'] == 'success')
      {
        alert(`Product added Successfully`)
      }
      else
      {
        console.log(response['error'])
      }
    })
  }
  onCancel()
  {

  }
}
