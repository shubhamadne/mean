import { ProductService } from '../product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products = []

  productService : ProductService
  constructor(productService : ProductService) 
  {
    this.productService = productService
  }

  ngOnInit(): void {
    this.loadProducts()
  }

  loadProducts()
  {
    const request = this.productService.getProduct()
    request.subscribe(response => {
      if(response['status'] == 'success')
      {
        this.products = response['data']
      }
      else
      {
        console.log(response['error'])
      }
    })
  }

  onEdit(product)
  {

  }
  onDelete(product)
  {
    this.productService.deleteProduct(product['id']).subscribe(response => {
      if(response['status'] == 'success')
      {
        alert(`Product deleted Successfully`)
        this.loadProducts()
      }
      else
      {
        console.log(response['error'])
      }
    })
  }
}
