import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = ''
  password = ''

  httpClient : HttpClient
  constructor( httpClient : HttpClient ) 
  {  
    this.httpClient = httpClient 
  }

  onLogin()
  { 
    const body = {
      email : this.email,
      password : this.password
    }
    const url = 'http://localhost:3000/user/signin'

    const request = this.httpClient.post(url,body)

    request.subscribe(response => {
      console.log(response)
      if(response['status'] == 'success')
      {
        const data = response['data']
        const firstName = data['firstName']
        const lastName = data['lastName']

        alert(`Welcome ${firstName} ${lastName}`)
      }
      else
      {
        alert(`Invalid Email or Password`)
      }
    })
  }
  onCancel()
  { }

  ngOnInit(): void {
  }

}
