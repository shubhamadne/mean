const express = require('express')
const db = require('../db')
const utils = require('../utils')
const crypto = require('crypto-js')

const router = express.Router()

//signup
router.post('/signup', (request, response) => {
  const { firstName, lastName, address, city, phone, email, password } = request.body;

  const encryptedPassword = crypto.SHA256(password);

  const statement = `insert into user (firstName, lastName, address, city, phone, email, password) VALUES
                      ('${firstName}','${lastName}', '${address}','${city}','${phone}','${email}','${encryptedPassword}')`

  db.query(statement, (error, data) => {
     response.send(utils.createResult(error, data)); 
  })
})
//Signin
router.post('/signin', (request, response) => {
  const { email, password } = request.body;

  const statement = `select id, firstName, lastName, email, password from user where email = '${email}' and password = '${crypto.SHA256(password)}'`
  console.log(statement);
  db.query(statement, (error, admins) => {
      if(error)
      {
          response.send({status : 'error', error : error});
      }
      else{
          if(admins.length == 0)
          {
              response.send({status : 'error', error : 'Admin does not exists'});
          }
          else
          {
              const admin = admins[0];
              response.send(utils.createResult(error, {
                  firstName : admin['firstName'],
                  lastName : admin['lastName'],
                  email : admin['email'],
                  password : admin['password']
              }));
          }
      }
  })
})
module.exports = router