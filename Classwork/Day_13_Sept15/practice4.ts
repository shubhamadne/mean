class Person{
    protected name : string;
    protected age : number;
    protected address : string;

    public constructor(name : string = "", age : number = 0, address : string = "")
    {
        this.name = name;
        this.age = age;
        this.address = address;
    }
}

class Employee extends Person{
    protected empId : number;

    public constructor(name : string, age : number, address : string, empId : number = 0)
    {
        super(name, age, address);
        this.empId = empId;
    }
}

const p1 = new Person();
