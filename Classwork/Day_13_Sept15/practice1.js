var Student = /** @class */ (function () {
    function Student() {
    }
    Object.defineProperty(Student.prototype, "name", {
        // Getters
        get: function () {
            return this._name;
        },
        //Setters
        set: function (name) {
            this._name = name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "roll", {
        get: function () {
            return this._roll;
        },
        set: function (roll) {
            this._roll = roll;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "mark", {
        get: function () {
            return this._mark;
        },
        set: function (mark) {
            this._mark = mark;
        },
        enumerable: false,
        configurable: true
    });
    return Student;
}());
var student1 = new Student();
console.log(student1);
student1.name = "Shubham";
student1.roll = 36556;
student1.mark = 85.12;
console.log(student1);
console.log("Name = " + student1.name);
console.log("Name = " + student1.roll);
console.log("Name = " + student1.mark);
