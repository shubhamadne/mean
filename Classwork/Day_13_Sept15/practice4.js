var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Person = /** @class */ (function () {
    function Person(name, age, address) {
        if (name === void 0) { name = ""; }
        if (age === void 0) { age = 0; }
        if (address === void 0) { address = ""; }
        this.name = name;
        this.age = age;
        this.address = address;
    }
    return Person;
}());
var Employee = /** @class */ (function (_super) {
    __extends(Employee, _super);
    function Employee(name, age, address, empId) {
        if (empId === void 0) { empId = 0; }
        var _this = _super.call(this, name, age, address) || this;
        _this.empId = empId;
        return _this;
    }
    return Employee;
}(Person));
var p1 = new Person();
