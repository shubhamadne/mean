
class Student{

    private _name : string
    private _roll : number
    private _mark : number

    //Setters
    public set name(name : string)
    {
        this._name = name;
    }

    public set roll(roll : number)
    {
        this._roll = roll;
    }

    public set mark(mark : number)
    {
        this._mark = mark;
    }

    // Getters
    public get name()
    {
        return this._name;
    }

    public get roll()
    {
        return this._roll;
    }

    public get mark()
    {
        return this._mark
    } 
}

const student1 = new Student();
console.log(student1);
student1.name = "Shubham";
student1.roll = 36556;
student1.mark = 85.12;
console.log(student1);
console.log(`Name = ${student1.name}`);
console.log(`Name = ${student1.roll}`);
console.log(`Name = ${student1.mark}`);