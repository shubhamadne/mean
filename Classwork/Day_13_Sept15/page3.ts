class Person {
    private name: string
    private _age: number
    private address: string
  
    // setter
    public setName(name: string) {
      this.name = name
    }
  
    // setter method can be accessed as a property
    public set age(age: number) {
      this._age = age
    }
  
    public set Address(address: string) {
      this.address = address
    }
  
  
    // getter
    public getName() {
      return this.name
    }
  
    // getter method can be accessed as a property
    public get age() {
      return this._age
    }
  
    public get Address() {
      return this.address
    }
  }
  
  const p1 = new Person()
  p1.setName("person1")
  
  // setter property
  p1.age = 20
  p1.Address = "pune"
  
  console.log(`name = ${p1.getName()}`)
  console.log(`age = ${p1.age}`)
  console.log(`address = ${p1.Address}`)
  
  
  console.log(p1)