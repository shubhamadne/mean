interface Run {
    run()
  }
  
  class Animal implements Run {
    run() {
      console.log('animal is running')
    }
  }
  
  // contract between service provider and service consumer
  interface IDrawable {
    draw()
    erase()
  }
  
  // service provider
  class Rectangle implements IDrawable {
  
    draw() {
      console.log('drawing rectangle')
    }
    erase() {
      console.log('erasing rectangle')
    }
  
    rectangle() {
      console.log('inside rectangle')
    }
    
  }
  
  class Circle implements IDrawable {
  
    draw() {
      console.log('drawing circle')
    }
    erase() {
      console.log('erasing circle')
    }
    
  }
  
  class Square implements IDrawable {
  
    draw() {
      console.log('drawing square')
    }
    erase() {
      console.log('erasing square')
    }
    
  }
  
  //       type of reference = type of object
  const drawable1: IDrawable = new Rectangle()
  drawable1.draw()
  drawable1.erase()
  
  const drawable2: IDrawable = new Circle()
  drawable2.draw()
  drawable2.erase()
  
  const drawable3: IDrawable = new Square()
  drawable3.draw()
  drawable3.erase()