"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pi = exports.divide = exports.add = void 0;
function add(p1, p2) {
    console.log("Addition = " + (p1 + p2));
}
exports.add = add;
function divide(p1, p2) {
    console.log("Division = " + p1 / p2);
}
exports.divide = divide;
exports.pi = 3.14;
