import { Person } from './person'
import * as page1 from './page1'

const person = new Person("Shubham", "Pune", 27);
person.printInfo();
person.canVote();

page1.add(20, 15);
page1.divide(10,2);
console.log(`PI = ${page1.pi}`);