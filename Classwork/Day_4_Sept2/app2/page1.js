const mysql = require('mysql')

function getProducts() {
  // step1.1: create connection
  const connection = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: 'manager',
    database: 'PHP'
  })

  // step1.2: open the connection
  connection.connect()  //optional

  // step2: prepare SQL query
  const statement = `select id, title, price, description, category from products`

  // step3: execute the query
  // asynchronous
  connection.query(statement, (error, data) => {
    console.log(`error: ${error}`)

    // step4: process the result
    // console.log(data)
    // for (let index = 0; index < data.length; index++) {
    //   const row = data[index];
    
    //   console.log(`id => ${row['id']}`)
    //   console.log(`title => ${row['title']}`)
    //   console.log(`price => ${row['price']}`)
    //   console.log(`description => ${row['description']}`)
    //   console.log(`category => ${row['category']}`)
    //   console.log('----------------------------------------------------')
    // }
    console.table(data)

    // step5: close the connection
    connection.end()
  })

}

//getProducts()

function createProduct(title, description, price, category, userId, company) {
    // step1.1: create connection
    const connection = mysql.createConnection({
      host: '127.0.0.1',
      user: 'root',
      password: 'manager',
      database: 'PHP'
    })
  
    // step1.2: open the connection
    connection.connect()
  
    // step2: prepare SQL query
    const statement = `insert into products (title, price, description, category, userId, company) values ('${title}', '${price}', '${description}', '${category}', '${userId}', '${company}')`
  
    // step3: execute the query
    // asynchronous
    connection.query(statement, (error, data) => {
      if(error)
      {
        console.log(`error : ${error}`);
      }
      else{
        console.log(`Data inserted successfully..!!`);
      }
      console.table(data)
  
      // step5: close the connection
      connection.end()
    })
  
  }
  
 // createProduct("product 7", "this is a very nice product", 700, "kitchen", 1, 'lg')

 function updateProduct()
 {
   const id = 8;
   const title = 'Lenovo Laptop';

   const mysql = require('mysql');

   const connection = mysql.createConnection({

        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'PHP'
   })

   const query = `update products set title= '${title}' where id=${id}`

   connection.query(query, (error,data) => {

    if(error)
    {
      console.log(`error : ${error}`);
    }
    else{
      console.log(`Data with id : ${id} is updated successfully..!!`);
    }

    connection.end();
   })
 }
 //updateProduct();

 function deleteProduct()
 {
   const id = 10;

   const mysql = require('mysql');

   const connection = mysql.createConnection({

        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'PHP',
        port : 3306
   })

   const query = ` delete from products where id=${id}`

   connection.query(query, (error, data) => {
     if(error)
     {
       console.log(`error : ${error}`);
     }
     else{
       console.log(`Data with id : ${id} is deleted successfully..!!`);
     }

     connection.end();
   })
 }

 deleteProduct();