// step1: require the http package
// used to create the http server or http client
const http = require('http')

// step2: create the server process
const server = http.createServer((request, response) => {
  console.log('request has been received')

  // url: the path (page) client is requesting
  console.log(`url = ${request.url}`)

  // method: http methond client has used while sending the request
  console.log(`method = ${request.method}`)

  // read the response as a string
  // response.setHeader('Content-Type', 'text/plain')

  // read the response as json
  // response.setHeader('Content-Type', 'application/json')

  // read the response as html
  response.setHeader('Content-Type', 'text/html')

  response.end('[{ "name": "steve", "company": "Apple" }, { "name": "bill", "company": "MS" }]')
})

// step3: start the server 
// listen on a port number 3000
server.listen(3000, '0.0.0.0', () => {
  console.log('server started listening on port 3000')
})