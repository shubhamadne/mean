const { response } = require("express")

function createResult(error, data) {
  return error ? createError(error) : createSucccess(data)
}

function createSucccess(data) {
  const result = {}
  result['status'] = 'success'
  result['data'] = data

  return result
}

function createError(error) {
  const result = {}
  result['status'] = 'error'
  result['error'] = error

  return result
}

module.exports = {
  createResult: createResult,
  createError: createError,
  createSucccess: createSucccess
}