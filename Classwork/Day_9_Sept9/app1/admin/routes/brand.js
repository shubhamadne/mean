const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/', (request, response) => {
  const statement = `select id, title, description from brand`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/', (request, response) => {
  const {title, description} = request.body
  const statement = `insert into brand (title, description) values ('${title}', '${description}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/:brandId', (request, response) => {
  const { brandId } = request.params;
  const { title, description } = request.body;

  const statement = `update brand set title = '${title}', description = '${description}' WHERE id = '${brandId}'`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:brandId', (request, response) => {
  const { brandId } = request.params;

  const statement = `delete from brand where id = ${brandId}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router