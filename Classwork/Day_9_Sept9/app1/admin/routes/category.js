const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/', (request, response) => {
  const statement = `select id, title, description from category`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post('/', (request, response) => {
  const {title, description} = request.body
  const statement = `insert into category (title, description) values ('${title}', '${description}')`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put('/:Id', (request, response) => {
  const { Id } = request.params;
  const { title, description } = request.body;

  const statement = `update category set title = '${title}', description = '${description}' WHERE id = '${Id}'`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:Id', (request, response) => {
  const { Id } = request.params;

  const statement = `delete from category where id = ${Id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------

module.exports = router