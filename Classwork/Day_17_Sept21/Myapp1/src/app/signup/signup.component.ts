import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  firstName = ''
  lastName = ''
  city = ''
  address = ''
  phone = ''
  email = ''
  password = ''


  httpClient : HttpClient
  constructor(httpClient : HttpClient) 
  { 
    this.httpClient = httpClient 
  }

  onSignup()
  {
    const body = {
      firstName : this.firstName,
      lastName : this.lastName,
      city : this.city,
      address : this.address,
      phone : this.phone,
      email : this.email,
      password : this.password
    }

    const url = 'http://localhost:3000/user/signup'

   const request = this.httpClient.post(url,body)
   request.subscribe(response => {
     console.log(response)
     if(response['status'] == 'success')
     {
       const data = response['data']

       alert(`You Have registered Successfully..!!`)
     }
     else
     {
       alert(`Signup failed..!!`)
     }
   })
  }
  onCancel()
  {

  }
  ngOnInit(): void {
  }

}
