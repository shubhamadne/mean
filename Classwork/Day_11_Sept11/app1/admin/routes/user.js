const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /user:
 *   get:
 *     description: For getting list of all users
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */
router.get('/getAllUser', (request, response) => {
  const statement = `select * from user`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------
/**
 * @swagger
 *
 * /user:
 *   get:
 *     description: For getting list of brands
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */
router.get('', (request, response) => {
  response.send()
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------
/**
 * @swagger
 *
 * /user:
 *   put:
 *     description: For updating user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */
router.put('/:userId/:userState', (request, response) => {
    const { userId, userState } = request.params;
    const statement = `update user set active = ${userState} WHERE id = ${userId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------
/**
 * @swagger
 *
 * /user:
 *   delete:
 *     description: For deleteing user
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: successful message
 */
router.delete('/:userId', (request, response) => {
    const { userId } = request.params;
    const statement = `delete from user WHERE id = ${userId}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

// ----------------------------------------------------

module.exports = router