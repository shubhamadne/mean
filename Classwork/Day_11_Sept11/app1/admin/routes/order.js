const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()
// ----------------------------------------------------
// GET
// ----------------------------------------------------

//Get all orders by all Users
router.get('/allOrders', (request, response) => {
  
  const statement = `select * from orderDetails`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

//Get all orders by Specific user
router.get('/', (request, response) => {
  
  const statement = `select * from userOrder WHERE userId = 5`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.get('', (request, response) => {
  response.send()
})

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

//Update Payment Status and delivery status
router.put('/updateStatus', (request, response) => {
  
  const {paymentStatus, deliveryStatus, userId } = request.body;
  const statement = `update userOrder set paymentStatus = '${paymentStatus}', 
                     deliveryStatus = '${deliveryStatus}' WHERE userId = ${userId}`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, 'Status Updated'))
  })
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/:userId', (request, response) => {

  const statement = `delete from userOrder WHERE userId = ${userId}`

  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, 'Deleted Successfully'))
  })
})

// ----------------------------------------------------

module.exports = router