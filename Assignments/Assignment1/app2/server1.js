
const express = require('express');

const app = express();

// For customer
app.get('/customer', (request, response) => {
    console.log(`request received`);
    console.log(`select * from customer`);
    response.end('from GET /customer');
})
app.post('/customer', (request, response) => {
    console.log(`request received`);
    console.log(`insert into customer...`);
    response.end('from POST /customer');
})
app.put('/customer', (request, response) => {
    console.log(`request received`);
    console.log(`update customer set ....`);
    response.end('from PUT /customer');
})
app.delete('/customer', (request, response) => {
    console.log(`request received`);
    console.log(`delete from customer .....`);
    response.end('from DELETE /customer');
})

// For Pizza item
app.get('/pizza', (request, response) => {
    console.log(`request received`);
    console.log(`select * from pizza_item`);
    response.end('from GET /pizza');
})
app.post('/pizza', (request, response) => {
    console.log(`request received`);
    console.log(`insert into pizza_item...`);
    response.end('from POST /pizza');
})
app.put('/pizza', (request, response) => {
    console.log(`request received`);
    console.log(`update pizza_item set ....`);
    response.end('from PUT /pizza');
})
app.delete('/pizza', (request, response) => {
    console.log(`request received`);
    console.log(`delete from pizza_item .....`);
    response.end('from DELETE /pizza');
})
app.listen(3000, '0.0.0.0', () => {
    console.log('You are listening to port 3000');
})