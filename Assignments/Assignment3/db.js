//create mysql require
const mysql = require('mysql2')

//create connection 
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'manager',
    database: 'MEAN',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
  });
  
  //exports the pool and added/imported into user,note module
  module.exports = pool