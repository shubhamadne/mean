//function checking error and result
function createResult(error, dbResult)
{
    const result = { status :''}
    if(error)
    { //error while performing
        result['status'] = 'error'
        result['error'] = error
    }
    else {
        //exceution successful
        result['status'] = 'success'
        result['data'] = dbResult
    }
    return result

}

//export module
module.exports = {
    createResult : createResult
}