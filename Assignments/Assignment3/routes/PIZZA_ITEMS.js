const express = require('express')

//adding db and utils module
const db = require('../db')
const utils = require('../utils')

//Router for routing middleware
const router = express.Router()

//router object for routing GET method and app called internally router
router.get('/', (request, response) => {
    const id = request.body.id

    const statement = `select * from pizza_item where id = '${id}' `

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })

})

  //POST METHOD
  router.post('/', (request, response) => {
    const name = request.body.name
    const type = request.body.type
    const category = request.body.category
    const description = request.body.decsription
    
    const statement = `insert into pizza_item (name,type,category,description)
         values ('${name}','${type}','${category}','${description}')`

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })

})

  //PUT Method
  router.put('/', (request, response) => {
    const category = request.body.category
    const id = request.body.id
    
    const statement = `update pizza_item set category = '${category}' where id = '${id}'`

    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })

})

  //DELETE Method
  router.delete('/', (request, response) => {
    const id = request.body.id

    const statement = `delete from pizza_item where id = '${id}'`
    
    db.query(statement, (error, dbResult) => {
        response.send(utils.createResult(error, dbResult))
    })

})


//the exported router can be imported and added into the server
module.exports = router