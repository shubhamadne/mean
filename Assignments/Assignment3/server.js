//create express
const express = require('express')

//parsing request body contents into JSON object
const bodyParser = require('body-parser')

//importing customer 
const customerRouter = require('./routes/Customer')

//importing pizza_items 
const pizza_itemsRouter = require('./routes/PIZZA_ITEMS')

//use express instance
const app = express()

app.use(bodyParser.json())

//add routes to the apllication
app.use('/Customer', customerRouter)

app.use('/PIZZA_ITEMS', pizza_itemsRouter)


app.listen(4000,'0.0.0.0',() => {
    console.log('Server Started on Port 4000')
})