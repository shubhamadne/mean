const express = require('express');

const mysql = require('mysql');

const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.get('/customer', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })

    const query = `select * from customer`

    connection.query(query, (error, data) => {

        connection.end();

        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(data);
        }
    })
})


app.post('/customer', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })
  
    const query = `insert into customer (name, passward, mobile, address, email) 
                   VALUES
                   ('${request.body.name}',
                   '${request.body.passward}',
                   '${request.body.mobile}',
                   '${request.body.address}',
                   '${request.body.email}')`
    
    connection.query(query, (error, result) => {
  
        connection.end();
  
        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(result);
        }
    })
})

app.put('/customer', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })
  
    const query = `update customer 
                   set name = '${request.body.name}',
                       passward = '${request.body.passward}',
                       mobile = '${request.body.mobile}',
                       address = '${request.body.address}',
                       email = '${request.body.email}'
                    WHERE id = ${request.body.id}`
                    
    
    connection.query(query, (error, result) => {
  
        connection.end();
  
        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(result);
        }
    })
})

app.delete('/customer', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })
  
    const query = `delete from customer where id = ${request.body.id}`
                    
    
    connection.query(query, (error, result) => {
  
        connection.end();
  
        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(result);
        }
    })
})
app.listen(3000, '0.0.0.0', () => {
    console.log(`You are reading from port 3000`);
})