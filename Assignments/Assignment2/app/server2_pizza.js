
const express = require('express');

const mysql = require('mysql');

const app = express();

const bodyParser = require('body-parser');
const { request } = require('http');
const { response } = require('express');
app.use(bodyParser.json());

app.get('/pizza', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })

    const query = `select * from pizza_item`

    connection.query(query, (error, data) => {

        connection.end();

        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(data);
        }
    })
})

app.post('/pizza', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })
  
    const query = `insert into pizza_item (name, type, category, description) 
                   VALUES
                   ('${request.body.name}',
                   '${request.body.type}',
                   '${request.body.category}',
                   '${request.body.description}')`
    
    connection.query(query, (error, result) => {
  
        connection.end();
  
        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(result);
        }
    })
})

app.put('/pizza', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })
  
    const query = `update pizza_item 
                   set name = '${request.body.name}',
                       type = '${request.body.type}',
                       category = '${request.body.category}',
                       description = '${request.body.description}'
                    WHERE id = ${request.body.id}`
                    
    
    connection.query(query, (error, result) => {
  
        connection.end();
  
        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(result);
        }
    })
})

app.delete('/pizza', (request, response) => {

    const connection = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'manager',
        database : 'MEAN'
    })
  
    const query = `delete from pizza_item where id = ${request.body.id}`
                    
    
    connection.query(query, (error, result) => {
  
        connection.end();
  
        if(error)
        {
            console.log(`Error : ${error}`);
        }
        else
        {
            response.send(result);
        }
    })
})

app.listen(3000, '0.0.0.0', () => {
    console.log(`You are reading from port 3000`);
})