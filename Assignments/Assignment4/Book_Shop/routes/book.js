const express = require('express')
const utils = require('../utils')
const db = require('../db')
const router = express.Router()

// -----------------------GET-----------------------------
//Get distinct subject
router.get('/getDistinctBooks', (request, response) => {
  
    const query = `select DISTINCT subject from books`

    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})
//find book by subject
router.get('/:Subject', (request, response) => {
    const { Subject } = request.params;

    const query = `select name from books WHERE subject = '${Subject}'`

    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})
//Get All Books
router.get('/getAllBooks', (request, response) => {

    const query = `select * from books`
  
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})
//Get Book By Id
router.get('/:bookId', (request, response) => {
    const { bookId } = request.params;

    const query = `select * from books WHERE id = '${bookId}'`

    db.query(query, (error, data) => {
        response.send(utils.createResult(error, data));
    })
})
// ------------------------POST----------------------------
//Add Book
router.post('/', (request, response) => {
    const { name, author, subject, price } = request.body;

    const query = `insert into books (name, author, subject, price) VALUES 
                    ('${name}','${author}','${subject}','${price}')`
    
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
  })

// --------------------------PUT--------------------------

//Edit Book
router.put('/:bookId', (request, response) => {
    const { bookId } = request.params;

    const { name, birth, address, mobile, email, password } = request.body;

    const query = `update customers set name = '${name}', author = '${author}', subject ='${subject}', 
                    price = '${price}' WHERE id = '${bookId}'` 
    
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})

// --------------------------DELETE--------------------------

//Delete Book
router.get('/:bookId', (request, response) => {
    const { bookId } = request.params;

    const query = `delete from customers WHERE id = '${bookId}'` 
    
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})

// ----------------------------------------------------

module.exports = router