const express = require('express');
const utils = require('../utils');
const db = require('../db');
const { request } = require('http');
const router = express.Router();

// ----------------------GET------------------------------
//Get All User
router.get('/getAllUsers', (request, response) => {

  const query = `select * from customers`

  db.query(query, (error,data) => {
      response.send(utils.createResult(error,data));
  })
})
//Get User By Id
router.get('/:userId', (request, response) => {
    const { userId } = request.params;

    const query = `select * from customers WHERE id = '${userId}'`

    db.query(query, (error, data) => {
        response.send(utils.createResult(error, data));
    })
})
//Find user by email and password
router.get('/', (request, response) => {
    const { email, password } = request.body;

    const query = `select * from customers WHERE email = '${email}' and password = '${password}'`

    db.query(query, (error, data) => {
        response.send(utils.createResult(error,data));
    })
})


// -------------------------POST---------------------------
//Add User
router.post('/', (request, response) => {
    const { name, birth, address, mobile, email, password } = request.body;

    const query = `insert into customers (name, password, mobile, address, email, birth) VALUES 
                    ('${name}','${password}','${mobile}','${address}','${email}','${birth}')`
    
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
  })


// -------------------------PUT---------------------------
//Edit User
router.put('/:userId', (request, response) => {
    const { userId } = request.params;

    const { name, birth, address, mobile, email, password } = request.body;

    const query = `update customers set name = '${name}', password = '${password}', mobile ='${mobile}', 
                    address = '${address}', email = '${email}', birth = '${birth}' WHERE id = '${userId}'` 
    
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})


// -------------------------DELETE---------------------------
//Delete User
router.get('/:userId', (request, response) => {
    const { userId } = request.params;

    const query = `delete from customers WHERE id = '${userId}'` 
    
    db.query(query, (error,data) => {
        response.send(utils.createResult(error,data));
    })
})

// ----------------------------------------------------

module.exports = router