const express = require('express');
const bodyParser = require('body-parser');

const userRouter = require('./routes/user');
const bookRouter = require('./routes/book');

const app = express();
app.use(bodyParser.json());

app.use('/user', userRouter);
app.use('/book', bookRouter);

app.get('/', (request, response) => {
    response.send('Welcome to My Bookshop Application');
})
app.listen(3000, '0.0.0.0', () => {
    console.log('You are listening to port 3000');
})